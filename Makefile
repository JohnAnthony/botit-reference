.phony: tidy

all: book.pdf tidy

tidy:
	rm -f build/*.{log,aux,toc}

book.pdf: book.tex *.png
	pdflatex --halt-on-error book.tex
